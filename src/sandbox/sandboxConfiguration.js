import path from "path";

/**
 * @property {string} unattendedExecutionAccount
 * @property {DataAdapterConfiguration} adapter
 */
class SandboxConfiguration {
    constructor() {
        this.unattendedExecutionAccount = 'sandboxAdmin';
        this.adapter = {
            invariantName: 'sqlite',
            name: 'sandbox',
            options: {
                database: path.resolve(__dirname, '../../db/universis.db')
            }
        }
    }
}

export {SandboxConfiguration};
